package com.example.tp1android;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import data.Country;

public  class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {

        String uri = Country.countries[i].getImgUri();
        Context c1 = holder.image.getContext();
        Context c2 = holder.image.getContext();
        holder.image.setImageDrawable(c1.getResources().getDrawable(c1.getResources().getIdentifier(uri,
                "drawable", c1.getPackageName())));

    }

    @Override
    public int getItemCount() {
        return Country.countries.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView pays;
        public  TextView capital;


        public ViewHolder(View view) {
            super(view);
            image=view.findViewById(R.id.image);
            pays =  view.findViewById(R.id.text1);
            capital = view.findViewById(R.id.text2);


            int position = getAdapterPosition();

            view.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    int position = getAdapterPosition();



                }
            });
        }





    }}
